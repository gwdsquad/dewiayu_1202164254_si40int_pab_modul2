package com.example.marveltravel;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;
import android.util.Log;

public class SplashScreen extends AppCompatActivity{
    TextView tvSplash;
//    Log.d("SplashScreen", "ini onStart");
//        Log.d("Splashscreen ", "ini onResume");
//        Log.d("OpenGLRenderer", "Render dirty regions requested: true");
//        Log.d("Atlas", "Validating map...");
//        Log.d("OpenGLRenderer", "Enabling debug mode 0");
//        Log.d("SplashScreen", "ini onPause");
//        Log.d("MainActivity", "ini onStart");
//        Log.d("Mainactivity ", "ini onResume");
//        Log.d("SplashScreen", "ini onStop");
//        Log.d("Splashscreen", "ini onDestroy");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashScreen.this,Main2Activity.class));
                finish();
            }
        },2000);

    }
}
