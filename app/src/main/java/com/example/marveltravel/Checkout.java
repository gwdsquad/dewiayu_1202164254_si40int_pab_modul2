package com.example.marveltravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class Checkout extends AppCompatActivity {

    String nominalsaldo;
    int sisasaldo;
    Button Beli;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        TextView tujuan = (TextView) findViewById(R.id.nilaitujuan);
        TextView tanggalberangkat = (TextView) findViewById(R.id.tgB);
        TextView tanggalpulang = (TextView) findViewById(R.id.tgP);
        TextView waktuberangkat = (TextView) findViewById(R.id.wkB);
        TextView waktupulang = (TextView) findViewById(R.id.wkP);
        TextView harga = (TextView) findViewById(R.id.harga);
        TextView jumlahtiket = (TextView) findViewById(R.id.JumlahTiket);
        Beli = (Button) findViewById(R.id.beli);


        /**
         * Kita cek apakah ada Bundle atau tidak
         */
        if(getIntent().getExtras()!=null){
            /**
             * Jika Bundle ada, ambil data dari Bundle
             */
            Bundle bundle = getIntent().getExtras();
            tujuan.setText(bundle.getString("tujuan"));
            tanggalberangkat.setText(bundle.getString("tanggalB"));
            tanggalpulang.setText(bundle.getString("tanggalP"));
            waktuberangkat.setText(bundle.getString("waktuB"));
            waktupulang.setText(bundle.getString("waktuP"));
            harga.setText(bundle.getString("hargatiket"));
            jumlahtiket.setText(bundle.getString("jumlahtiket"));
            jumlahtiket.setText(bundle.getString("jumlahtiket"));
        }else{
            /**
             * Apabila Bundle tidak ada, ambil dari Intent
             */
            Bundle bundle = getIntent().getExtras();
            tujuan.setText(getIntent().getStringExtra("tujuan"));
            tanggalberangkat.setText(getIntent().getStringExtra("tanggalB"));
            tanggalpulang.setText(getIntent().getStringExtra("tanggalP"));
            waktuberangkat.setText(getIntent().getStringExtra("waktuB"));
            waktupulang.setText(getIntent().getStringExtra("waktuP"));
            harga.setText(getIntent().getStringExtra("hargatiket"));
            jumlahtiket.setText(getIntent().getStringExtra("jumlahtiket"));
        }


        Beli.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                //int receiveValue = getIntent().getIntExtra("nominalsaldo", 0);
                Intent intent = new Intent(Checkout.this, Main2Activity.class);

                startActivity(intent);
            }

        });


    }
}