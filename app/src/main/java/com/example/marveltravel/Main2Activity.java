package com.example.marveltravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.app.Dialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.DatePicker;
import android.app.DatePickerDialog;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import android.view.View.OnClickListener;
import android.widget.TimePicker;
import android.app.TimePickerDialog;
import android.text.format.DateFormat;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Spinner;
import android.widget.Toast;


public class Main2Activity extends AppCompatActivity {

    TextView btTimePickerB, btTimePickerP, jumlahsaldo, btShow;
    EditText nominal;
    String tanggalberangkat, waktuberangkat, sisasaldo;
    String tanggalpulang, waktupulang;
    public int nominalsaldo =0;
    public int nominaltopup, saldobaru;
    public int hargatiket, jumlahtiket;
    public Dialog dialog;
    public Spinner spNamen;
    public DatePickerDialog datePickerDialogB, datePickerDialogP;
    public TimePickerDialog timePickerDialogB, timePickerDialogP;
    public SimpleDateFormat dateFormatterP, dateFormatterB ;
    public Switch mySwitch;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main2);

            Bundle extras = getIntent().getExtras();


            btShow = (TextView) findViewById(R.id.topup_btn);

            TextView tanggal_berangkat = (TextView) findViewById(R.id.tanggal_berangkat);
            btTimePickerB = (TextView) findViewById(R.id.waktu_berangkat);
            final TextView tanggal_Pulang = (TextView) findViewById(R.id.tanggal_pulang);
            btTimePickerP = (TextView) findViewById(R.id.waktu_pulang);
            jumlahsaldo=(TextView) findViewById(R.id.saldo);




            mySwitch = (Switch) findViewById(R.id.switch1);
            mySwitch.setChecked(true);

            TextView harga = (TextView) findViewById(R.id.harga);

            spNamen = (Spinner) findViewById(R.id.Sptujuan);


            Button konfirmasiByr = (Button) findViewById(R.id.konfirmasi);
            konfirmasiByr.setOnClickListener(new OnClickListener(){
                public void onClick(View arg0) {
                    String Tujuan = spNamen.getSelectedItem().toString();
                    if ("Jakarta (Rp 85.000)".equals(Tujuan)) {
                        hargatiket = 85000;
                    } else if ("Cirebon (Rp 150.000)".equals(Tujuan)) {
                        hargatiket = 150000;
                    } else if ("Bekasi (Rp 70.000)".equals(Tujuan)) {
                        hargatiket = 70000;
                    }
                    EditText inputjumlah = (EditText) findViewById(R.id.jumlahTiket);
                    jumlahtiket = Integer.parseInt(inputjumlah.getText().toString());
                    if (mySwitch.isChecked()) {
                        hargatiket = (hargatiket * jumlahtiket *2);
                    } else {
                        hargatiket = (hargatiket * jumlahtiket);
                        //switchStatus.setText("Status: OFF");
                    }
                    if (hargatiket <= nominalsaldo) {

                        //Toast.makeText(getApplicationContext(), "pilihan anda " + hargatiket, Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(Main2Activity.this, Checkout.class);
                        //intent.putExtra("waktup", hargatiket);
                        intent.putExtra("hargatiket", "Rp. " + hargatiket);
                        intent.putExtra("tujuan", Tujuan);
                        intent.putExtra("tanggalP", tanggalpulang);
                        intent.putExtra("tanggalB", tanggalberangkat);
                        intent.putExtra("waktuB", waktuberangkat);
                        intent.putExtra("waktuP", waktupulang);
                        intent.putExtra("jumlahtiket", "" + jumlahtiket);
                        intent.putExtra("nominalsaldo", nominalsaldo);
                        nominalsaldo = (hargatiket-nominalsaldo);
                        saldobaru = nominalsaldo;

                        startActivity(intent);
                    }else {
                        Toast.makeText(getApplicationContext(), "Maaf, Saldo anda kurang. silahkan Top Up saldo dulu!", Toast.LENGTH_LONG).show();
                    }


                }});

            mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {

                    if (isChecked) {

                        tanggal_Pulang.setText("Pilih Tanggal");
                        btTimePickerP.setText("Pilih Waktu");
                        hargatiket = hargatiket*2;
                    } else {
                        tanggal_Pulang.setText("");
                        btTimePickerP.setText("");
                    }

                }
            });

            //check the current state before we display the screen
            if(mySwitch.isChecked()){
                tanggal_Pulang.setText("Pilih Tanggal");
                btTimePickerP.setText("Pilih Waktu");
                hargatiket = hargatiket*2;
            }
            else {
                //switchStatus.setText("Status: OFF");
                tanggal_Pulang.setText("");
                btTimePickerP.setText("");
            }


            btTimePickerB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTimeDialogB();
                }
            });
            btTimePickerP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTimeDialogP();
                }
            });

            btShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**
                     * Initiate Custom Dialog
                     */
                    dialog = new Dialog(Main2Activity.this);
                    dialog.setContentView(R.layout.topup);
                    dialog.setTitle("Masukan Jumlah Saldo");

                    EditText nominal = (EditText) findViewById(R.id.nominaltopup) ;

                    Button cancelButton = (Button) dialog.findViewById(R.id.bt_cancel);
                    Button submitButton = (Button) dialog.findViewById(R.id.bt_submit);
                    /**
                     * Jika tombol diklik, tutup dialog
                     */
                    submitButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            EditText edit=(EditText)dialog.findViewById(R.id.nominaltopup);
                            int text = Integer.valueOf(edit.getText().toString());

                            nominalsaldo = (text+nominalsaldo);
                            jumlahsaldo.setText("Rp "+nominalsaldo);
                            dialog.dismiss();
                        }
                    });
                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            });
            tanggal_berangkat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDateDialogB();
                }
            });
            tanggal_Pulang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDateDialogP();
                }
            });

        }
        public void showDateDialogB(){

            /**
             * Calendar untuk mendapatkan tanggal sekarang
             */
            Calendar newCalendar = Calendar.getInstance();

            /**
             * Initiate DatePicker dialog
             */
            datePickerDialogB = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                    /**
                     * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                     */

                    /**
                     * Set Calendar untuk menampung tanggal yang dipilih
                     */
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);

                    /**
                     * Update TextView dengan tanggal yang kita pilih
                     */

                    //BUKA PADA WAKTUNYA
                    TextView tvDateResultB = (TextView) findViewById(R.id.htBerangkat);
                    dateFormatterB = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

                    //Intent intent = new Intent(MainActivity.this, CHECKOUT.class);
                    //intent.putExtra("tanggalb", dateFormatterB.format(newDate.getTime()));
                    tvDateResultB.setText((""+dateFormatterB.format(newDate.getTime())));
                    tanggalberangkat = (""+dateFormatterB.format(newDate.getTime()));

                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

            /**
             * Tampilkan DatePicker dialog
             */
            datePickerDialogB.show();
        }
        public void showDateDialogP(){

            /**
             * Calendar untuk mendapatkan tanggal sekarang
             */
            Calendar newCalendarP = Calendar.getInstance();

            /**
             * Initiate DatePicker dialog
             */
            datePickerDialogP = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                    /**
                     * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                     */

                    /**
                     * Set Calendar untuk menampung tanggal yang dipilih
                     */
                    Calendar newDatep = Calendar.getInstance();
                    newDatep.set(year, monthOfYear, dayOfMonth);

                    /**
                     * Update TextView dengan tanggal yang kita pilih
                     */

                    //BUKA PADA WAKTUNYA
                    TextView tvDateResultP = (TextView) findViewById(R.id.htPulang);
                    dateFormatterP = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                    //Intent intent = new Intent(MainActivity.this, CHECKOUT.class);
                    //intent.putExtra("tanggalp", dateFormatterP.format(newDate.getTime()));
                    tvDateResultP.setText((""+dateFormatterP.format(newDatep.getTime())));
                    tanggalpulang = (""+dateFormatterP.format(newDatep.getTime()));

                }

            },newCalendarP.get(Calendar.YEAR), newCalendarP.get(Calendar.MONTH), newCalendarP.get(Calendar.DAY_OF_MONTH));

            /**
             * Tampilkan DatePicker dialog
             */
            datePickerDialogP.show();
        }
        public void showTimeDialogB() {

            /**
             * Calendar untuk mendapatkan waktu saat ini
             */
            Calendar calendar = Calendar.getInstance();

            /**
             * Initialize TimePicker Dialog
             */
            timePickerDialogB = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    /**
                     * Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                     */
                    //BUKA PADA WAKTUNYA
                    TextView tvTimeResultB = (TextView) findViewById(R.id.hwBerangkat);
                    //Intent intent = new Intent(MainActivity.this, CHECKOUT.class);

                    tvTimeResultB.setText(""+hourOfDay+":"+minute);
                    waktuberangkat = (""+hourOfDay+":"+minute);
                }
            },
                    /**
                     * Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                     */
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),

                    /**
                     * Cek apakah format waktu menggunakan 24-hour format
                     */
                    DateFormat.is24HourFormat(this));

            timePickerDialogB.show();
        }
        public void showTimeDialogP() {

            /**
             * Calendar untuk mendapatkan waktu saat ini
             */
            Calendar calendar = Calendar.getInstance();

            /**
             * Initialize TimePicker Dialog
             */
            timePickerDialogP = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    /**
                     * Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                     */

                    //BUKA PADA WAKTUNYA
                    TextView tvTimeResultP = (TextView) findViewById(R.id.hwPulang);
                    Intent intent = new Intent(Main2Activity.this, Checkout.class);
                    //intent.putExtra("waktup", ""+hourOfDay+":"+minute);
                    tvTimeResultP.setText(""+hourOfDay+":"+minute);
                    waktupulang = (hourOfDay+":"+minute);

                }
            },
                    /**
                     * Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                     */
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),

                    /**
                     * Cek apakah format waktu menggunakan 24-hour format
                     */
                    DateFormat.is24HourFormat(this));

            timePickerDialogP.show();
        }
        public void hitungbiaya(View v) {
            //EditText nama = (EditText) findViewById(R.id.Ednama);
            //String Nama = nama.getText().toString();

        }





    }
